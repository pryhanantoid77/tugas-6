import {
  ActivityIndicator,
  Button,
  FlatList,
  Image,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  RefreshControl,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from './url';
import Icon from 'react-native-vector-icons/dist/AntDesign';
import {useIsFocused} from '@react-navigation/native';
import {Modalform, modalform} from './ModalForm';
import Axios from 'axios';

const Home = ({navigation}) => {
  const [dataMobil, setDataMobil] = useState([]);
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const isFocused = useIsFocused();
  const [refreshing, setRefreshing] = useState(false);
  // const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    getDataMobil();
  }, [isFocused]);
  const getDataMobil = async () => {
    Axios.get(`${BASE_URL}mobil`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        console.log('res get mobil berhasil', response);
        if (response.status === 200) {
          console.log(response.data.items);
          setDataMobil(response.data.items);
        } else {
          if (response.status === 401 || response.status === 402) {
            return false;
          }
          if (response.status === 500) {
            return false;
          }
        }
      })
      // .then(response => {
      //   console.log('res get mobil', response);
      // })
      .catch(error => {
        console.log('error get mobil', error);
      });
    setRefreshing(false);
  };

  const postData = async () => {
    if (namaMobil == '') {
      alert('Nama Mobil Tidak Boleh Kosong');
    } else if (totalKM == '') {
      alert('Total Kilometer Tidak Boleh Kosong');
    } else if (hargaMobil == '') {
      alert('Harga Mobil Tidak Boleh Kosong');
    } else if (hargaMobil < 100000000) {
      alert('Harga Mobil Kurang dari 100JT');
    } else {
      setIsLoading(true);
      const body = [
        {
          title: namaMobil,
          harga: hargaMobil,
          totalKM: totalKM,
          unitImage:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
        },
      ];

      const options = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
      };
      Axios.post(`${BASE_URL}mobil`, body, options)
        .then(response => {
          console.log('response add succes', response);
          if (response.status === 200 || response.status === 201) {
            alert('Data Mobil berhasil ditambahkan');
            getDataMobil();
          } else {
            if (response.status === 401 || response.status === 402) {
              alert(response.status);
            }
            if (response.status === 500) {
              alert(response.status);
            }
          }
        })
        .catch(error => {
          console.log('error', error);
        });
      setIsLoading(false);
      setShowModal(!showModal);
    }
  };

  const convertCurrency = (nominal = 0, currency) => {
    // 10000, 'Rp'
    let rupiah = '';
    const nominalref = nominal.toString().split('').reverse().join('');
    for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
        rupiah += nominalref.substr(i, 3) + '.';
      }
    }

    if (currency) {
      return (
        // Rp. 10.000
        currency +
        rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')
      );
    } else {
      return rupiah //10.000
        .split('', rupiah.length - 1)
        .reverse()
        .join('');
    }
  };
  const handleRefresh = () => {
    setRefreshing(true);
    getDataMobil();
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Modal
        // animationType={'slide'}
        transparent={true}
        visible={isLoading}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        {/*All views of Modal*/}
        {/*Animation can be slide, slide, none*/}
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
          }}>
          <View
            style={{
              width: '80%',
              height: 300,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 10,
              marginTop: 80,
              marginHorizontal: 40,
            }}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
      <Modal
        // animationType={'slide'}
        transparent={true}
        visible={showModal}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        {/*All views of Modal*/}
        {/*Animation can be slide, slide, none*/}
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
          }}>
          <View
            style={{
              width: '80%',
              height: '60%',
              backgroundColor: 'white',
              // justifyContent: 'center',
              // alignItems: 'center',
              borderRadius: 10,
              marginTop: 80,
              marginHorizontal: 40,
            }}>
            <ScrollView>
              <View
                style={{
                  // width: '100%',
                  padding: 15,
                }}>
                <View>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Nama Mobil
                  </Text>
                  <TextInput
                    value={namaMobil}
                    onChangeText={text => setNamaMobil(text)}
                    placeholder="Masukkan Nama Mobil"
                    style={styles.txtInput}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Total Kilometer
                  </Text>
                  <TextInput
                    value={totalKM}
                    onChangeText={text => setTotalKM(text)}
                    placeholder="contoh: 100 KM"
                    style={styles.txtInput}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Harga Mobil
                  </Text>
                  <TextInput
                    value={hargaMobil}
                    onChangeText={text => setHargaMobil(text)}
                    placeholder="Masukkan Harga Mobil"
                    style={styles.txtInput}
                    keyboardType="number-pad"
                  />
                </View>
                <TouchableOpacity
                  onPress={() => postData()}
                  //   onPress={() => <Loading />}
                  style={styles.btnAdd}>
                  <Text style={{color: '#fff', fontWeight: '600'}}>
                    Tambah Data
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.btnAdd}
                  onPress={() => setShowModal(!showModal)}>
                  <Text style={{color: 'white'}}> Go Back</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>

      <Text
        style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
        Home screen
      </Text>
      <FlatList
        data={dataMobil}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={handleRefresh} />
        }
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('Detail', item)}
            // onPress={() => setShowModal(!showModal)}
            activeOpacity={0.8}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 15,
              borderColor: '#dedede',
              borderWidth: 1,
              borderRadius: 6,
              padding: 12,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: '30%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{width: '90%', height: 100, resizeMode: 'contain'}}
                source={{
                  uri:
                    typeof item?.unitImage === 'string' ? item.unitImage : '',
                }}
              />
            </View>
            <View
              style={{
                width: '70%',
                paddingHorizontal: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Nama Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}> {item.title}</Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Total KM :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {item.totalKM}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Harga Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000', width: '70%'}}>
                  {convertCurrency(item.harga, 'Rp. ')}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 30,
          right: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => setShowModal(!showModal)}>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
