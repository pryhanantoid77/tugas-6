import {
  ActivityIndicator,
  Image,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from './url';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/dist/Ionicons';

const Detail = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [imageMobil, setImageMobil] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);

  var dataMobil = route.params;

  useEffect(() => {
    console.log('log123', dataMobil);
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
      typeof setImageMobil(data?.unitImage) === 'string'
        ? setImageMobil(data.unitImage)
        : '';
    }
  }, []);

  const editData = async () => {
    setIsLoading(true);
    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    const options = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    };
    Axios.put(`${BASE_URL}mobil`, body, options)
      .then(response => {
        console.log('response edit succes', response);
        if (response.status === 200 || response.status === 201) {
          alert('Data Mobil berhasil diedit');
          navigation.navigate('Home');
        } else {
          if (response.status === 401 || response.status === 402) {
            alert(response.status);
          }
          if (response.status === 500) {
            alert(response.status);
          }
        }
      })
      .catch(error => {
        console.log('error', error);
      });

    setIsLoading(false);
    setShowModal(!showModal);
  };

  const deleteData = async () => {
    setIsLoading(true);

    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];
    // try {
    //   const response = await fetch(`${BASE_URL}mobil`, {
    //     method: 'DELETE',
    //     headers: {
    //       'Content-Type': 'application/json',
    //       Authorization: TOKEN,
    //     },
    //     body: JSON.stringify(body),
    //   });

    //   const result = await response.json();
    //   alert('Data Mobil berhasil dihapus');
    //   navigation.navigate('Home');
    // } catch (error) {
    //   console.error('Error:', error);
    // }
    const options = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    };
    console.log('1', options);
    console.log('token', TOKEN);
    Axios.delete(`${BASE_URL}mobil`, {data: body, ...options})
      .then(response => {
        console.log('response hapus succes', response);
        if (response.status === 200 || response.status === 201) {
          alert('Data Mobil berhasil dihapus');
          navigation.navigate('Home');
        } else {
          if (response.status === 401 || response.status === 402) {
            alert(response.status);
          }
          if (response.status === 500) {
            alert(response.status);
          }
        }
      })
      .catch(error => {
        console.log('error', error);
      });
    setIsLoading(false);
  };

  const convertCurrency = (nominal = 0, currency) => {
    // 10000, 'Rp'
    let rupiah = '';
    const nominalref = nominal.toString().split('').reverse().join('');
    for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
        rupiah += nominalref.substr(i, 3) + '.';
      }
    }

    if (currency) {
      return (
        // Rp. 10.000
        currency +
        rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')
      );
    } else {
      return rupiah //10.000
        .split('', rupiah.length - 1)
        .reverse()
        .join('');
    }
  };

  return (
    <View style={{flex: 1}}>
      <Modal
        // animationType={'slide'}
        transparent={true}
        visible={isLoading}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        {/*All views of Modal*/}
        {/*Animation can be slide, slide, none*/}
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
          }}>
          <View
            style={{
              width: '80%',
              height: 300,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 10,
              marginTop: 80,
              marginHorizontal: 40,
            }}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
      <Modal
        // animationType={'slide'}
        transparent={true}
        visible={showModal}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        {/*All views of Modal*/}
        {/*Animation can be slide, slide, none*/}
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
          }}>
          <View
            style={{
              width: '80%',
              height: '60%',
              backgroundColor: 'white',
              // justifyContent: 'center',
              // alignItems: 'center',
              borderRadius: 10,
              marginTop: 80,
              marginHorizontal: 40,
            }}>
            <ScrollView>
              <View
                style={{
                  // width: '100%',
                  padding: 15,
                }}>
                <View>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Nama Mobil
                  </Text>
                  <TextInput
                    value={namaMobil}
                    onChangeText={text => setNamaMobil(text)}
                    placeholder="Masukkan Nama Mobil"
                    style={styles.txtInput}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Total Kilometer
                  </Text>
                  <TextInput
                    value={totalKM}
                    onChangeText={text => setTotalKM(text)}
                    placeholder="contoh: 100 KM"
                    style={styles.txtInput}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Harga Mobil
                  </Text>
                  <TextInput
                    value={hargaMobil}
                    onChangeText={text => setHargaMobil(text)}
                    placeholder="Masukkan Harga Mobil"
                    style={styles.txtInput}
                    keyboardType="number-pad"
                  />
                </View>
                <TouchableOpacity
                  onPress={() => editData()}
                  //   onPress={() => <Loading />}
                  style={styles.btnAdd}>
                  <Text style={{color: '#fff', fontWeight: '600'}}>
                    Edit Data
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.btnAdd}
                  onPress={() => setShowModal(!showModal)}>
                  <Text style={{color: 'white'}}> Go Back</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
      <Image
        source={{uri: imageMobil}}
        style={{height: '40%', width: '100%'}}
      />
      <View
        style={{
          // flexDirection: 'row',
          marginTop: -250,
          // justifyContent: 'space-between',
          marginHorizontal: 20,
          height: 250,
          // backgroundColor: 'aqua',
          // width: '100%',
        }}>
        <Icon
          name="arrow-back"
          size={25}
          color="white"
          onPress={() => navigation.goBack('')}
        />
      </View>
      <View
        style={{
          backgroundColor: 'white',
          marginTop: -20,
          height: '100%',
          borderRadius: 20,
          padding: 20,
        }}>
        <Text>Nama Mobil : {namaMobil}</Text>
        <Text>Total Kilometer : {totalKM}</Text>
        <Text>Harga Mobil : {convertCurrency(hargaMobil, 'Rp. ')}</Text>
        <TouchableOpacity
          onPress={() => setShowModal(!showModal)}
          //   onPress={() => <Loading />}
          style={{
            marginTop: 20,
            width: '100%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: '#689f38',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#fff', fontWeight: '600'}}>Edit Data</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => deleteData()}
          style={{
            marginTop: 20,
            width: '100%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: 'red',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#fff', fontWeight: '600'}}>Hapus Data</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
